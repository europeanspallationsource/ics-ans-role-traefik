ics-ans-role-traefik
====================

Ansible role to install traefik.

[Traefik](https://docs.traefik.io) is a modern HTTP reverse proxy and load balancer
made to deploy microservices with ease.

This role configures traefik with HTTP redirect to HTTPS, Let's Encrypt support
and Docker backend.

For traefik to redirect traffic to your docker container, you should:

- add your container to the `traefik_network` network
- expose the port to use
- set the following labels:
    * traefik.enable: "true"
    * traefik.backend: "{name}"
    * traefik.port: {port}
    * traefik.frontend.rule: "Host:{host}"
    * traefik.docker.network: "{traefik_network}"

The `traefik.frontend.rule` is set to `Host:{containerName}.{domain}` by default.
It's important to set the `traefik.docker.network` if your container is linked to several networks.
Otherwise it will randomly pick one.
The `traefik.port` label is not needed if you expose only one port. See the [traefik.toml documentation](https://docs.traefik.io/toml/#docker-backend)
for more information.

Here is an example using the [docker_container](https://docs.ansible.com/ansible/docker_container_module.html)
module from Ansible:

```yaml
- name: launch my container
  docker_container:
    name: mycontainer
    ...
    purge_networks: yes
    networks:
      - name: "{{traefik_network}}"
    labels:
      traefik.enable: "true"
      traefik.backend: "mycontainer"
      traefik.port: "{{mycontainer_port | int}}"
      traefik.frontend.rule: "Host:mycontainer.example.com"
      traefik.docker.network: "{{traefik_network}}"
```

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
traefik_version: 1.3
traefik_network: traefik-network
traefik_debug: "false"
# Accepted values, in order of severity: "DEBUG", "INFO", "WARN", "ERROR", "FATAL", "PANIC"
traefik_log_level: ERROR
# Dashboard only available from localhost by default
# Set to 8080 if you want to access it from another machine
traefik_dashboard_port: 127.0.0.1:8080
traefik_use_letsencrypt: false
traefik_domain: esss.lu.se
traefik_letsencrypt_email: ""
# Use the let's encrypt staging server by default
# Set to true to use the production environment
traefik_letsencrypt_production: false
traefik_wildcard_cert_rpm: http://install01.esss.lu.se/extra_repo/wildcardcertesss-1.8-1.noarch.rpm
traefik_cert_file: /etc/ssl/certs/esss.lu.se.chained.crt
traefik_key_file: /etc/ssl/certs/esss.lu.se.key
traefik_self_signed_cert: false
traefik_cert_subject: "/C=SE/ST=Skane/L=Lund/O=ESSS/CN={{ansible_fqdn}}"
```

To enable let's encrypt, set `traefik_use_letsencrypt` to true.
By default, the ESS wildcard certificate will be installed and the files `traefik_cert_file` and `traefik_key_file` will be used.
You can set `traefik_self_signed_cert` to true to generate a sef-signed certificate. Note that you probably want to change the
`traefik_cert_file` and `traefik_key_file` variables in that case.

The traefik dashboard is only available from localhost by default.
Set `traefik_dashboard_port` to 8080 if you want to access it from another machine.

This role uses the let's encrypt staging server by default.
Set `traefik_letsencrypt_production` to true to use the production environment.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-traefik
```

License
-------

BSD 2-clause
