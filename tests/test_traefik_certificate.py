import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('certificate_group')


def test_traefik_certificate_config(host):
    config = host.file('/etc/traefik/traefik.toml').content_string
    assert '[acme]' not in config
    assert '[[entryPoints.https.tls.certificates]]' in config


def test_installed_certificate(host):
    assert host.file('/etc/ssl/certs/esss.lu.se.chained.crt').exists
    assert host.file('/etc/ssl/certs/esss.lu.se.key').exists


def test_traefik_debug_true(host):
    with host.sudo():
        cmd = host.run('docker logs traefik_proxy')
    assert 'level=info msg="Starting server on :443"' in cmd.stderr
    assert 'level=debug msg="Provider connection established with docker' in cmd.stderr
