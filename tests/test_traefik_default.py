import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('default_group')


def test_traefik_letsencrypt_config(host):
    config = host.file('/etc/traefik/traefik.toml').content_string
    assert '[acme]' in config
    assert '[[entryPoints.https.tls.certificates]]' not in config


def test_no_generated_certificate(host):
    assert not host.file('/etc/ssl/certs/esss.lu.se.chained.crt').exists
    assert not host.file('/etc/ssl/certs/esss.lu.se.key').exists


def test_traefik_debug_false(host):
    with host.sudo():
        cmd = host.run('docker logs traefik_proxy')
    assert 'level=debug' not in cmd.stderr
