import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_traefik_running(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['traefik_proxy']


def test_traefik_dashboard(host):
    cmd = host.run('curl http://localhost:8080')
    assert 'href="/dashboard/"' in cmd.stdout
