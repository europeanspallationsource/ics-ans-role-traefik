import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('self_signed')


def test_self_signed_certificate(host):
    assert host.file('/etc/traefik/ssl/traefik.crt').exists
    assert host.file('/etc/traefik/ssl/traefik.key').exists
